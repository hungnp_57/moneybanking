package app.bills;

public class Bill {

	String money = null;
	String name = null;
	String time = null;
	boolean selected = false;

	public Bill(String name, String money, String time, boolean selected) {
		super();
		this.name = name;
		this.money = money;
		this.time = time;
		this.selected = selected;
	}

	public String getMoney() {
		return money;
	}

	public void setMoney(String money) {
		this.money = money;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}
}