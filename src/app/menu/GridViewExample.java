package app.menu;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.ImageButton;
import app.main.Login;
import app.main.R;

public class GridViewExample extends Activity implements OnItemClickListener {

	GridView gridView;

	static final String[] GRID_DATA = new String[] { "Tài khoản",
			"Chuyển tiền", "Nạp thẻ", "Thanh toán", "Tỷ giá", "ATM", "Hóa đơn" };
	String s, t;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.grid_view_android_example);
		ActionBar actionBar = getActionBar();
		if (actionBar != null) {
			actionBar.setTitle("Tùy chọn chức năng");
			actionBar.setDisplayHomeAsUpEnabled(true);

		}
		getActionBar().setBackgroundDrawable(
				new ColorDrawable(Color.parseColor("#3cb879")));
		Intent callerIntent = getIntent();
		Bundle packageFromCaller = callerIntent.getBundleExtra("LoginInfo");
		s = packageFromCaller.getString("user");
		t = packageFromCaller.getString("PIN");
		ImageButton help = (ImageButton) findViewById(R.id.action_help);
		ImageButton chat = (ImageButton) findViewById(R.id.action_chat);
		ImageButton setting = (ImageButton) findViewById(R.id.action_setting);
		chat.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intentHelp = new Intent(GridViewExample.this, Chat.class);
				startActivity(intentHelp);
			}
		});
		setting.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intentSetting = new Intent(GridViewExample.this,
						Setting.class);
				Bundle bundle = new Bundle();
				bundle.putString("user", s);
				intentSetting.putExtra("IntentSetting", bundle);
				startActivity(intentSetting);
			}
		});
		help.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				AlertDialog.Builder b = new AlertDialog.Builder(
						GridViewExample.this);
				b.setTitle("Thông tin ứng dụng");
				b.setMessage("\t\t\tUET Money Banking\n\t\t\tPhiên bản 1.0\n\t\t\tLatest version");
				b.setPositiveButton("OK",
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog,
									int which) {

							}
						});
				b.create().show();
			}
		});

		gridView = (GridView) findViewById(R.id.gridView1);

		gridView.setAdapter(new CustomGridAdapter(this, GRID_DATA));

		gridView.setOnItemClickListener(this);

	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.logout, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			finish();
			return true;
		case R.id.logout:
			finish();
			Intent quit=new Intent(GridViewExample.this, Login.class);
			quit.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(quit);
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		// TODO Auto-generated method stub
		switch (arg2) {
		case 0:
			Intent intentInfo = new Intent(this, Info.class);
			Bundle bundle = new Bundle();
			bundle.putString("user", s);
			intentInfo.putExtra("IntentInfo", bundle);
			startActivity(intentInfo);
			break;
		case 1:
			Intent intentTransfer = new Intent(this, Transfer.class);
			Bundle bundle2 = new Bundle();
			bundle2.putString("user", s);
			intentTransfer.putExtra("IntentTransfer", bundle2);
			startActivity(intentTransfer);
			break;
		case 2:
			Intent intentRefill = new Intent(this, Refill.class);
			Bundle bundle3 = new Bundle();
			bundle3.putString("user", s);
			intentRefill.putExtra("IntentRefill", bundle3);
			startActivity(intentRefill);
			break;
		case 3:
			Intent intentPayments = new Intent(this, Payments.class);
			Bundle bundle4 = new Bundle();
			bundle4.putString("user", s);
			intentPayments.putExtra("IntentPayments", bundle4);
			startActivity(intentPayments);
			break;
		case 4:
			Intent intentConversion = new Intent(this, Conversion.class);
			startActivity(intentConversion);
			break;
		case 5:
			Intent intentATM = new Intent(this, ATM.class);
			startActivity(intentATM);
			break;
		case 6:
			Intent intentBills = new Intent(this, Bills.class);
			Bundle bundle5 = new Bundle();
			bundle5.putString("user", s);
			intentBills.putExtra("IntentBills", bundle5);
			startActivity(intentBills);

		}

	}

}