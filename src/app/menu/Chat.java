package app.menu;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Vector;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import app.chat.Message;
import app.chat.MessagesListAdapter;
import app.main.Login;
import app.main.R;

public class Chat extends Activity {

	private Button btnSend;
	private EditText inputMsg;

	// Chat messages list adapter
	private MessagesListAdapter adapter;
	private List<Message> listMessages;
	private ListView listViewMessages;

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.chat);
		final Vector<String> s = new Vector<String>();
		s.add("Quý khách có thể vui lòng mô tả chi tiết hơn?");
		s.add("Anh có thể mua 3 cây kem ăn 1 lúc");
		s.add("Không thành vấn đề xD");
		s.add("LOL");
		s.add("Thật không may, quý khách không thể thực hiện bây giờ :(");
		s.add("Sẽ rất tốt nếu quý khách không chat nữa xD");
		s.add("Quý khách chat buồn ngủ quá!");

		ActionBar actionBar = getActionBar();
		getActionBar().setBackgroundDrawable(
				new ColorDrawable(Color.parseColor("#3cb879")));
		if (actionBar != null) {
			actionBar.setTitle("Hỗ trợ trực tuyến");
			actionBar.setDisplayHomeAsUpEnabled(true);

		}

		btnSend = (Button) findViewById(R.id.btnSend);
		inputMsg = (EditText) findViewById(R.id.inputMsg);
		listViewMessages = (ListView) findViewById(R.id.list_view_messages);
		listMessages = new ArrayList<Message>();

		adapter = new MessagesListAdapter(this, listMessages);
		listViewMessages.setAdapter(adapter);
		Message m = new Message(
				"Hệ thống",
				"Đây là dịch vụ hỗ trợ trực tuyến. \nMọi thắc mắc của quý khách sẽ được chúng tôi giải quyết sớm nhất",
				false);
		appendMessage(m);
		btnSend.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Message m = new Message("Me", inputMsg.getText().toString(),
						true);

				// Appending the message to chat list
				appendMessage(m);
				inputMsg.setText("");
				Random r = new Random();
				int i = r.nextInt(7);
				Message n = new Message("Madolche Anjelly", s.get(i), false);

				// Appending the message to chat list
				appendMessage(n);

			}
		});

	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.logout, menu);
		return true;
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			finish();
			return true;
		case R.id.logout:
			finish();
			Intent quit=new Intent(Chat.this, Login.class);
			quit.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(quit);
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	private void appendMessage(final Message m) {
		runOnUiThread(new Runnable() {

			@Override
			public void run() {
				listMessages.add(m);

				adapter.notifyDataSetChanged();

				// Playing device's notification
				playBeep();
			}
		});
	}

	/**
	 * Plays device's default notification sound
	 * */
	public void playBeep() {

		try {
			Uri notification = RingtoneManager
					.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
			Ringtone r = RingtoneManager.getRingtone(getApplicationContext(),
					notification);
			r.play();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	final protected static char[] hexArray = "0123456789ABCDEF".toCharArray();

	public static String bytesToHex(byte[] bytes) {
		char[] hexChars = new char[bytes.length * 2];
		for (int j = 0; j < bytes.length; j++) {
			int v = bytes[j] & 0xFF;
			hexChars[j * 2] = hexArray[v >>> 4];
			hexChars[j * 2 + 1] = hexArray[v & 0x0F];
		}
		return new String(hexChars);
	}
}
