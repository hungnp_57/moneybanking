package app.menu;

import java.util.ArrayList;
import java.util.HashMap;

import listviewpayments.DataHelper;
import listviewpayments.Hoadon;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import app.bills.Bill;
import app.main.Login;
import app.main.R;

public class Bills extends Activity {
	private DataHelper dbHelper;
	ArrayList<Hoadon> arrHoadon;
	private ListView lvHoadon;
	String s;
	MyCustomAdapter dataAdapter = null;
	ArrayList<Bill> billList = new ArrayList<Bill>();
	ArrayList<HashMap<String, String>> array;
	int dem = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		ActionBar actionBar = getActionBar();
		if (actionBar != null) {
			actionBar.setTitle("Hóa đơn");
			actionBar.setDisplayHomeAsUpEnabled(true);

		}
		getActionBar().setBackgroundDrawable(
				new ColorDrawable(Color.parseColor("#3cb879")));

		super.onCreate(savedInstanceState);
		setContentView(R.layout.payments_list);
		dbHelper = new DataHelper(Bills.this);
		Intent callerIntent = getIntent();
		Bundle packageFromCaller = callerIntent.getBundleExtra("IntentBills");
		s = packageFromCaller.getString("user");

		arrHoadon = dbHelper.viewAll();
		lvHoadon = (ListView) findViewById(R.id.list);
		listTake();
	}

	private class MyCustomAdapter extends ArrayAdapter<Bill> {

		private ArrayList<Bill> billList;

		public MyCustomAdapter(Context context, int textViewResourceId,
				ArrayList<Bill> billList) {
			super(context, textViewResourceId, billList);
			this.billList = new ArrayList<Bill>();
			this.billList.addAll(billList);
		}

		private class ViewHolder {
			TextView name;
			TextView money;
			TextView time;
			CheckBox select;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {

			ViewHolder holder = null;

			if (convertView == null) {
				LayoutInflater vi = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				convertView = vi.inflate(R.layout.payments_list_item, null);

				holder = new ViewHolder();
				holder.name = (TextView) convertView.findViewById(R.id.from);
				holder.money = (TextView) convertView
						.findViewById(R.id.subject);
				holder.time = (TextView) convertView.findViewById(R.id.date);
				holder.select = (CheckBox) convertView
						.findViewById(R.id.checkbox_list);
				convertView.setTag(holder);

				holder.select.setOnClickListener(new View.OnClickListener() {
					public void onClick(View v) {
						CheckBox cb = (CheckBox) v;
						Bill bill = (Bill) cb.getTag();

						/*
						 * Toast.makeText( getApplicationContext(),
						 * "Clicked on Checkbox: " + cb.getText() + " is " +
						 * cb.isChecked(), Toast.LENGTH_LONG) .show();
						 */
						bill.setSelected(cb.isChecked());
					}
				});
			} else {
				holder = (ViewHolder) convertView.getTag();
			}

			final Bill bill = billList.get(position);
			holder.name.setText(bill.getName());
			holder.money.setText(bill.getMoney());
			holder.time.setText(bill.getTime());
			holder.select.setTag(bill);
			holder.select
					.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

						@Override
						public void onCheckedChanged(CompoundButton buttonView,
								boolean isChecked) {
							bill.setSelected(isChecked);
							notifyDataSetChanged();
						}
					});
			holder.select.setChecked(bill.isSelected());

			return convertView;

		}

	}

	public void listTake() {

		// hien thi list cac take
		// lay du lieu tu cac bang trong CSDL hien thi ra listview
		arrHoadon = dbHelper.viewAll();

		billList.clear();
		for (int i = arrHoadon.size() - 1; i >= 0; i--) {
			if (arrHoadon.get(i).getUser().equals(s)) {
				Bill bill = new Bill(arrHoadon.get(i).getName(), arrHoadon.get(
						i).getMoney(), arrHoadon.get(i).getTime(), false);
				billList.add(bill);
			}
		}
		// create an ArrayAdaptar from the String Array
		dataAdapter = new MyCustomAdapter(this, R.layout.payments_list_item,
				billList);
		// Assign adapter to ListView
		lvHoadon.setAdapter(dataAdapter);

		/*
		 * array = new ArrayList<HashMap<String, String>>();
		 * 
		 * for (int i = arrHoadon.size() - 1; i >= 0; i--) { HashMap<String,
		 * String> temp = new HashMap<String, String>(); temp.put("hd_time",
		 * "Tháng " + arrHoadon.get(i).getTime()); temp.put("hd_money",
		 * arrHoadon.get(i).getMoney()); temp.put("hd_description",
		 * arrHoadon.get(i).getName()); array.add(temp); }
		 * 
		 * // init adapter String[] from = { "hd_description", "hd_money",
		 * "hd_time" };
		 * 
		 * int[] to = { R.id.from, R.id.subject, R.id.date };
		 * 
		 * adapter = new SimpleAdapter(Bills.this, array,
		 * R.layout.payments_list_item, from, to); //
		 * lvHoadon.setAdapter(adapter);
		 */

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			finish();
			return true;
		case R.id.logout:
			finish();
			Intent quit=new Intent(Bills.this, Login.class);
			quit.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(quit);
			return true;
		case R.id.delete:

			ArrayList<Bill> billList = dataAdapter.billList;
			for (int i = 0; i < billList.size(); i++) {
				Bill bill = billList.get(i);
				if (bill.isSelected()) {
					dbHelper.delete(bill.getTime());

					dem++;
				}
			}
			if (dem == 0) {
				Toast.makeText(getApplicationContext(),
						"Không có hóa đơn nào được chọn!", Toast.LENGTH_SHORT)
						.show();
			} else {
				Toast.makeText(getApplicationContext(),
						"Đã xóa " + dem + " hóa đơn", Toast.LENGTH_SHORT)
						.show();
			}
			dem = 0;
			dataAdapter.clear();
			arrHoadon = dbHelper.viewAll();

			billList.clear();
			for (int i = arrHoadon.size() - 1; i >= 0; i--) {
				if (arrHoadon.get(i).getUser().equals(s)) {
					Bill bill = new Bill(arrHoadon.get(i).getName(), arrHoadon.get(
							i).getMoney(), arrHoadon.get(i).getTime(), false);
					billList.add(bill);
				}
			}
			// create an ArrayAdaptar from the String Array
			dataAdapter = new MyCustomAdapter(this,
					R.layout.payments_list_item, billList);
			// Assign adapter to ListView
			lvHoadon.setAdapter(dataAdapter);
			lvHoadon.refreshDrawableState();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	protected void onDestroy() {
		super.onDestroy();
		dbHelper.close();
	}
}
