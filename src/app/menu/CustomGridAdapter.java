package app.menu;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import app.main.R;

public class CustomGridAdapter extends BaseAdapter {

	private Context context;
	private final String[] gridValues;

	// Constructor to initialize values
	public CustomGridAdapter(Context context, String[] gridValues) {
		this.context = context;
		this.gridValues = gridValues;
	}

	@Override
	public int getCount() {

		// Number of times getView method call depends upon gridValues.length
		return gridValues.length;
	}

	@Override
	public Object getItem(int position) {

		return null;
	}

	@Override
	public long getItemId(int position) {

		return 0;
	}

	// Number of times getView method call depends upon gridValues.length

	public View getView(int position, View convertView, ViewGroup parent) {

		// LayoutInflator to call external grid_item.xml file

		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		View gridView;

		if (convertView == null) {

			gridView = new View(context);

			// get layout from grid_item.xml
			gridView = inflater.inflate(R.layout.grid_item, null);

			// set value into textview

			TextView textView = (TextView) gridView
					.findViewById(R.id.grid_item_label);
			textView.setText(gridValues[position]);

			// set image based on selected text

			ImageView imageView = (ImageView) gridView
					.findViewById(R.id.grid_item_image);
			String item = gridValues[position];

			if (item.equals("Chuyển tiền")) {

				imageView.setImageResource(R.drawable.image1);

			} else if (item.equals("Tài khoản")) {

				imageView.setImageResource(R.drawable.image2);

			} else if (item.equals("Nạp thẻ")) {

				imageView.setImageResource(R.drawable.image3);

			} else if (item.equals("Thanh toán")) {

				imageView.setImageResource(R.drawable.image4);

			} else if (item.equals("Tỷ giá")) {

				imageView.setImageResource(R.drawable.image5);

			} else if (item.equals("ATM")) {

				imageView.setImageResource(R.drawable.image6);
			}
			else if (item.equals("Hóa đơn")) {

				imageView.setImageResource(R.drawable.image7);
			}
			
		} else {
			gridView = (View) convertView;
		}
		return gridView;
	}
}
