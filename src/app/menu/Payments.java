package app.menu;

import android.app.ActionBar;
import android.app.TabActivity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TabHost;
import android.widget.TabHost.TabSpec;
import app.main.Login;
import app.main.R;

@SuppressWarnings("deprecation")
public class Payments extends TabActivity {
	String s;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		ActionBar actionBar = getActionBar();
		if (actionBar != null) {
			actionBar.setTitle("Thanh toán");
			actionBar.setDisplayHomeAsUpEnabled(true);

		}
		getActionBar().setBackgroundDrawable(
				new ColorDrawable(Color.parseColor("#3cb879")));

		super.onCreate(savedInstanceState);
		setContentView(R.layout.payments);

		Intent callerIntent = getIntent();
		Bundle packageFromCaller = callerIntent
				.getBundleExtra("IntentPayments");
		s = packageFromCaller.getString("user");

		TabHost tabHost = (TabHost) findViewById(android.R.id.tabhost);

		TabSpec tab1 = tabHost.newTabSpec("First Tab");
		TabSpec tab2 = tabHost.newTabSpec("Second Tab");
		TabSpec tab3 = tabHost.newTabSpec("Third tab");

		// Set the Tab name and Activity
		// that will be opened when particular Tab will be selected
		tab1.setIndicator("Internet");
		Intent intentTab1 = new Intent(this, Tab1Activity.class);
		Bundle bundle = new Bundle();
		bundle.putString("user", s);
		intentTab1.putExtra("IntentTab1", bundle);
		tab1.setContent(intentTab1);

		tab2.setIndicator("Tiền điện");
		Intent intentTab2 = new Intent(this, Tab2Activity.class);
		Bundle bundle2 = new Bundle();
		bundle2.putString("user", s);
		intentTab2.putExtra("IntentTab2", bundle2);
		tab2.setContent(intentTab2);

		tab3.setIndicator("Tiền nước");
		Intent intentTab3 = new Intent(this, Tab3Activity.class);
		Bundle bundle3 = new Bundle();
		bundle3.putString("user", s);
		intentTab3.putExtra("IntentTab3", bundle3);
		tab3.setContent(intentTab3);

		/** Add the tabs to the TabHost to display. */
		tabHost.addTab(tab1);
		tabHost.addTab(tab2);
		tabHost.addTab(tab3);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.menu_payments, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int i = item.getItemId();
		if (i == android.R.id.home) {
			finish();
			return true;
		} else if (i == R.id.history) {
			Intent newIntent = new Intent(this, Bills.class);
			Bundle bundle5 = new Bundle();
			bundle5.putString("user", s);
			newIntent.putExtra("IntentBills", bundle5);
			startActivity(newIntent);
			return true;
		} else if (i == R.id.logout) {
			finish();
			Intent quit = new Intent(Payments.this, Login.class);
			quit.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(quit);
			return true;
		} else {
			return super.onOptionsItemSelected(item);
		}
	}
}
