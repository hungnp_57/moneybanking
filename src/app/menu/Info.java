package app.menu;

import java.util.ArrayList;

import listviewpayments.Account;
import listviewpayments.DataHelperAccount;
import listviewpayments.DataHelperExchange;
import listviewpayments.Exchange;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import app.main.Login;
import app.main.R;

public class Info extends Activity {

	private DataHelperAccount dbHelper;
	private DataHelperExchange db2;
	ArrayList<Exchange> arrExchange;
	TextView lastExchange;
	TextView lastExchangeMoney;
	ArrayList<Account> arrAccount;

	protected void onCreate(Bundle savedInstanceState) {
		ActionBar actionBar = getActionBar();
		if (actionBar != null) {
			actionBar.setTitle("Thông tin");
			actionBar.setDisplayHomeAsUpEnabled(true);

		}
		getActionBar().setBackgroundDrawable(
				new ColorDrawable(Color.parseColor("#3cb879")));

		super.onCreate(savedInstanceState);
		setContentView(R.layout.info);
		lastExchange = (TextView) findViewById(R.id.lastExchange);
		lastExchangeMoney = (TextView) findViewById(R.id.lastExchange2);
		dbHelper = new DataHelperAccount(Info.this);
		db2 = new DataHelperExchange(Info.this);
		arrExchange = db2.viewAll();
		arrAccount = dbHelper.viewAll();

		TextView user = (TextView) findViewById(R.id.infoUser);
		TextView money = (TextView) findViewById(R.id.infoMoney);
		TextView name = (TextView) findViewById(R.id.infoName);
		TextView phone = (TextView) findViewById(R.id.infoPhone);
		TextView address = (TextView) findViewById(R.id.infoAddress);
		Intent callerIntent = getIntent();
		Bundle packageFromCaller = callerIntent.getBundleExtra("IntentInfo");
		String s = packageFromCaller.getString("user");
		user.setText(s);
		for(int i=0;i<arrExchange.size();i++){
			if(arrExchange.get(i).getUser().equals(s)){
				lastExchange.setText(arrExchange.get(i).getTime());
				lastExchangeMoney.setText(arrExchange.get(i).getMoney()+" VND");
			}
		}
		
		for (int i = 0; i < arrAccount.size(); i++) {
			if (s.equals(arrAccount.get(i).getUser())) {
				money.setText(arrAccount.get(i).getMoney()+" VND");
				name.setText(arrAccount.get(i).getName());
				phone.setText(arrAccount.get(i).getPhone());
				address.setText(arrAccount.get(i).getAddress());
			}
		}
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.logout, menu);
		return true;
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			finish();
			return true;
		case R.id.logout:
			finish();
			Intent quit=new Intent(Info.this, Login.class);
			quit.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(quit);
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}
	
	protected void onDestroy() {
		super.onDestroy();
		dbHelper.close();
		db2.close();
	}
}
