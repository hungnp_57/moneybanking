package app.menu;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import listviewpayments.Account;
import listviewpayments.DataHelper;
import listviewpayments.DataHelperAccount;
import listviewpayments.DataHelperExchange;
import listviewpayments.DataHelperPaymentDetail;
import listviewpayments.Exchange;
import listviewpayments.PaymentDetail;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import app.main.R;

public class Tab1Activity extends Activity {
	private DataHelperAccount dbHelper;
	private DataHelperPaymentDetail dbHelper2;
	private DataHelper dbHelper3;
	private DataHelperExchange dbHelper4;
	ArrayList<Exchange> arrExchange;
	ArrayList<Account> arrAccount;
	ArrayList<PaymentDetail> arrDetail;
	Button btnThanhtoan;
	TextView tab1_supplier, tab1_money1, tab1_total;
	String s;
	String status1, status2, status3;
	TextView tab1_month;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.tab1);
		btnThanhtoan = (Button) findViewById(R.id.btnThanhtoan);
	
		dbHelper = new DataHelperAccount(Tab1Activity.this);
		dbHelper2 = new DataHelperPaymentDetail(Tab1Activity.this);
		dbHelper3 = new DataHelper(Tab1Activity.this);
		dbHelper4 = new DataHelperExchange(Tab1Activity.this);
		arrExchange = dbHelper4.viewAll();
		tab1_month = (TextView) findViewById(R.id.tab1_month);
		arrDetail = dbHelper2.viewAll();
		arrAccount = dbHelper.viewAll();
		tab1_money1 = (TextView) findViewById(R.id.tab1_money1);
		tab1_supplier = (TextView) findViewById(R.id.tab1_supplier);
		tab1_total = (TextView) findViewById(R.id.tab1_total);
		Calendar calendar = Calendar.getInstance();
		Date date = calendar.getTime();
		SimpleDateFormat format = new SimpleDateFormat("MM");
		String formattedDate = format.format(date);
		tab1_month.setText(formattedDate);
		Intent callerIntent = getIntent();
		Bundle packageFromCaller = callerIntent.getBundleExtra("IntentTab1");
		s = packageFromCaller.getString("user");

		for (int i = 0; i < arrDetail.size(); i++) {
			if (arrDetail.get(i).getUser().equals(s)) {
				tab1_supplier.setText(arrDetail.get(i).getSupplier());
				tab1_money1.setText(arrDetail.get(i).getMoney1()+ " VND/tháng");
				tab1_total
						.setText(""
								+ Integer
										.parseInt(arrDetail.get(i).getMoney1())
								* 1.05+" VND");
			}
		}

		for (int i = 0; i < arrDetail.size(); i++) {
			if (arrDetail.get(i).getUser().equals(s)) {
				if (arrDetail.get(i).getStatus1().equals("Yes")) {
					btnThanhtoan.setVisibility(View.INVISIBLE);
					AlertDialog.Builder b = new AlertDialog.Builder(
							Tab1Activity.this);
					b.setTitle("Thông báo");
					b.setMessage("Hóa đơn đã được thanh toán");
					b.setPositiveButton("OK",
							new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog,
										int which) {

								}
							});

					b.setNegativeButton("Xem hóa đơn",
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog,
										int which)

								{
									startHistory();
								}

							});

					b.create().show();
				}
			}
		}
		btnThanhtoan.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				int tiengoc = 0;
				int tienmoi = 0;
				PaymentDetail temp2 = new PaymentDetail();
				for (int j = 0; j < arrDetail.size(); j++) {
					if (arrDetail.get(j).getUser().equals(s)) {
						int t = Integer.parseInt(arrDetail.get(j).getMoney1());
						tienmoi = (int) (t * 1.05);
					}
				}
				;
				for (int i = 0; i < arrAccount.size(); i++) {
					if (arrAccount.get(i).getUser().equals(s)) {
						tiengoc = Integer
								.parseInt(arrAccount.get(i).getMoney());
					}
				}
				if (tienmoi > tiengoc) {
					doToast();
				} else {
					Account temp1 = new Account();
					for (int i = 0; i < arrAccount.size(); i++) {
						if (arrAccount.get(i).getUser().equals(s)) {
							String k = "" + (tiengoc - tienmoi);
							temp1.setMoney(k);
							temp1.setAddress(arrAccount.get(i).getAddress());
							temp1.setName(arrAccount.get(i).getName());
							temp1.setPhone(arrAccount.get(i).getPhone());
							temp1.setPIN(arrAccount.get(i).getPIN());
						}
					}
					dbHelper.updateAccount(temp1, s);
					String currentDateandTime;
					SimpleDateFormat sdf = new SimpleDateFormat(
							"HH:mm:ss dd-MM-yyyy");
					currentDateandTime = sdf.format(new Date());
					dbHelper3.add("Thanh toán Internet", "" + tienmoi,
							currentDateandTime, s);
					btnThanhtoan.setVisibility(View.INVISIBLE);
					Noti();
					for (int i = 0; i < arrDetail.size(); i++) {
						if (arrDetail.get(i).getUser().equals(s)) {
							for (int j = 0; j < arrAccount.size(); j++) {
								if (arrAccount.get(i).getUser().equals(s)) {
									temp2.setStatus1("Yes");
									temp2.setMoney1(arrDetail.get(i)
											.getMoney1());
									temp2.setMoney2(arrDetail.get(i)
											.getMoney2());
									temp2.setMoney3(arrDetail.get(i)
											.getMoney3());
									temp2.setStatus3(arrDetail.get(i)
											.getStatus1());
									temp2.setStatus2(arrDetail.get(i)
											.getStatus2());
									temp2.setSupplier(arrDetail.get(i)
											.getSupplier());
								}
							}
						}
					}
					
					Exchange temp3=new Exchange();
					for(int i=0;i<arrExchange.size();i++){
						if(arrExchange.get(i).getUser().equals(s)){
							temp3.setTime(currentDateandTime);
							temp3.setMoney(tienmoi+"");
						}
					}
					dbHelper4.updateExchange(temp3, s);
					dbHelper2.updatePaymentDetail(temp2, s);
				}
			}
		});

	}

	protected void onDestroy() {
		super.onDestroy();
		dbHelper2.close();
		dbHelper3.close();
		dbHelper.close();
		dbHelper4.close();
	}

	public void startHistory() {
		Intent newIntent = new Intent(this, Bills.class);
		Bundle bundle5 = new Bundle();
		bundle5.putString("user", s);
		newIntent.putExtra("IntentBills", bundle5);
		startActivity(newIntent);
	}

	public void doToast() {
		Toast toast = Toast.makeText(this,
				"Tài khoản không đủ tiền thanh toán", Toast.LENGTH_SHORT);
		toast.show();
	}

	public void Noti() {
		Toast toast = Toast.makeText(this, "Thanh toán thành công",
				Toast.LENGTH_SHORT);
		toast.show();
	}
}
