package app.menu;

import java.util.ArrayList;
import java.util.Vector;

import listviewpayments.DataHelperPlace;
import listviewpayments.Place;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.MultiAutoCompleteTextView;
import android.widget.Toast;
import app.main.Login;
import app.main.R;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class ATM extends Activity {
	private DataHelperPlace dbHelper;
	ArrayList<Place> arrPlace;
	ChipsMultiAutoCompleteTextview mu;
	EditText etPlace;
	EditText etRadius;
	ListView lv;
	Button btnMap;
	Vector<String> v1 = new Vector<String>();
	static final LatLng HAMBURG = new LatLng(21.0371126, 105.78290105);
	private GoogleMap map;

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.atm);
		getActionBar().setBackgroundDrawable(
				new ColorDrawable(Color.parseColor("#3cb879")));
		etPlace = (EditText) findViewById(R.id.multiAutoCompleteTextView1);
		lv = (ListView) findViewById(R.id.place);
		mu = (ChipsMultiAutoCompleteTextview) findViewById(R.id.multiAutoCompleteTextView1);
		dbHelper = new DataHelperPlace(ATM.this);
		arrPlace = dbHelper.viewAll();
		map = ((MapFragment) getFragmentManager().findFragmentById(R.id.map))
				.getMap();
		Marker hamburg = map.addMarker(new MarkerOptions().position(HAMBURG)
				.title("Hamburg"));
		// Move the camera instantly to hamburg with a zoom of 15.

		map.moveCamera(CameraUpdateFactory.newLatLngZoom(HAMBURG, 16));

		// Zoom in, animating the camera.
		map.animateCamera(CameraUpdateFactory.zoomTo(15), 2000, null);

		ActionBar actionBar = getActionBar();
		if (actionBar != null) {
			actionBar.setTitle("Tìm kiếm ATM");
			actionBar.setDisplayHomeAsUpEnabled(true);

		}

		String[] arr = new String[arrPlace.size()];
		for (int i = 0; i < arrPlace.size(); i++) {
			arr[i] = arrPlace.get(i).getPlace();
		}
		String[] item = getResources().getStringArray(R.array.place);
		mu.setAdapter(new ArrayAdapter<String>(this,
				android.R.layout.simple_dropdown_item_1line, item));
		mu.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());

	}

	private GoogleMap.OnMyLocationChangeListener myLocationChangeListener = new GoogleMap.OnMyLocationChangeListener() {
		@Override
		public void onMyLocationChange(Location location) {
			LatLng loc = new LatLng(location.getLatitude(),
					location.getLongitude());
			Marker mMarker = map.addMarker(new MarkerOptions().position(loc));
			if (map != null) {
				map.animateCamera(CameraUpdateFactory.newLatLngZoom(loc, 16.0f));
			}
		}
	};
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.logout, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			finish();
			return true;
		case R.id.logout:
			finish();
			Intent quit=new Intent(ATM.this, Login.class);
			quit.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(quit);
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	public void buttonClicked(View v) {
		if (mu.getText().toString().equals("") == false) {
			arrPlace = dbHelper.viewAll();
			v1.clear();
			String place = mu.getText().toString();
			for (int i = 0; i < arrPlace.size(); i++) {
				if (arrPlace.get(i).getPlace().toLowerCase()
						.contains(place.toLowerCase())) {
					v1.add(arrPlace.get(i).getPlace());
				}
			}
			ArrayAdapter<String> adapter6 = new ArrayAdapter<String>(ATM.this,
					R.layout.mytextview, v1);
			lv.setAdapter(adapter6);
			lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
				public void onItemClick(AdapterView<?> arg0, View arg1,
						int arg2, long arg3) {
					for (int i = 0; i < arrPlace.size(); i++) {
						if (arrPlace.get(i).getPlace().equals(v1.get(arg2))) {
							LatLng position = new LatLng(arrPlace.get(i)
									.getLat(), arrPlace.get(i).getLng());
							Marker hamburg = map.addMarker(new MarkerOptions()
									.position(position)
									.title(arrPlace.get(i).getPlace())
									.icon(BitmapDescriptorFactory
											.fromResource(R.drawable.icon_atm2)));
							float[] results = new float[1];
							Location.distanceBetween(HAMBURG.latitude,HAMBURG.longitude,
							                position.latitude, position.longitude, results);
							map.moveCamera(CameraUpdateFactory.newLatLngZoom(
									position, 16));

							map.animateCamera(CameraUpdateFactory.zoomTo(15),
									2000, null);
							Toast.makeText(getApplicationContext(),
									(int)results[0]+" m theo đường chim bay", Toast.LENGTH_SHORT)
									.show();

						}
					}

				}
			});
			adapter6 = new ArrayAdapter<String>(ATM.this, R.layout.mytextview,
					v1);
			lv.setAdapter(adapter6);

		}
		if (mu.getText().toString().equals("") == true) {
			doToast();
		}
	}

	protected void onDestroy() {
		super.onDestroy();
		dbHelper.close();
	}

	public void doToast() {
		Toast toast = Toast.makeText(this, "Vui lòng nhập đầy đủ thông tin!",
				Toast.LENGTH_SHORT);
		toast.show();
	}

}
