package app.menu;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import listviewpayments.Account;
import listviewpayments.DataHelper;
import listviewpayments.DataHelperAccount;
import listviewpayments.DataHelperExchange;
import listviewpayments.Exchange;
import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import app.main.Login;
import app.main.R;

public class Refill extends Activity {
	String arr1[] = { "Mobifone", "Vinaphone", "Viettel", "Vietnamobile" };
	Integer arr2[] = { 20000, 50000, 100000, 200000, 500000 };
	ProgressDialog barProgressDialog;
	Handler updateBarHandler;
	private DataHelperAccount dbHelper;
	private DataHelper db2;
	private DataHelperExchange dbHelper3;
	ArrayList<Exchange> arrExchange;
	ArrayList<Account> arrAccount;
	EditText editPIN;
	Button btnCheck;
	CheckBox check;
	Button btn_Finish;
	Spinner spin1;
	Spinner spin2;
	String s;

	protected void onCreate(Bundle savedInstanceState) {

		ActionBar actionBar = getActionBar();
		if (actionBar != null) {
			actionBar.setTitle("Nạp thẻ");
			actionBar.setDisplayHomeAsUpEnabled(true);

		}
		getActionBar().setBackgroundDrawable(
				new ColorDrawable(Color.parseColor("#3cb879")));
		super.onCreate(savedInstanceState);
		setContentView(R.layout.refill);

		dbHelper = new DataHelperAccount(Refill.this);
		dbHelper3 = new DataHelperExchange(Refill.this);
		arrExchange = dbHelper3.viewAll();
		db2 = new DataHelper(Refill.this);
		arrAccount = dbHelper.viewAll();
		editPIN = (EditText) findViewById(R.id.Refill_PIN);
		btnCheck = (Button) findViewById(R.id.check_Refill);
		btn_Finish = (Button) findViewById(R.id.finish_Refill);
		check = (CheckBox) findViewById(R.id.checkPIN_Refill);
		Intent callerIntent = getIntent();
		Bundle packageFromCaller = callerIntent.getBundleExtra("IntentRefill");
		s = packageFromCaller.getString("user");

		updateBarHandler = new Handler();
		spin1 = (Spinner) findViewById(R.id.spinner1);
		spin2 = (Spinner) findViewById(R.id.spinner2);
		ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, arr1);
		ArrayAdapter<Integer> adapter2 = new ArrayAdapter<Integer>(this,
				android.R.layout.simple_spinner_item, arr2);
		adapter1.setDropDownViewResource(android.R.layout.simple_list_item_single_choice);
		adapter2.setDropDownViewResource(android.R.layout.simple_list_item_single_choice);
		spin1.setAdapter(adapter1);
		spin2.setAdapter(adapter2);
		spin1.setOnItemSelectedListener(new MyProcessEvent1());
		spin2.setOnItemSelectedListener(new MyProcessEvent2());

		final Dialog d = new Dialog(Refill.this);
		d.setContentView(R.layout.dialog);
		d.setTitle("Xác nhận giao dịch");
		final TextView d1 = (TextView) d.findViewById(R.id.dialog1_1);
		final TextView d3 = (TextView) d.findViewById(R.id.dialog1_3);
		final TextView d2 = (TextView) d.findViewById(R.id.dialog1_2);
		Button b1 = (Button) d.findViewById(R.id.dialog1_4);
		Button b2 = (Button) d.findViewById(R.id.dialog1_5);

		final Dialog g = new Dialog(Refill.this);
		g.setContentView(R.layout.dialog2);
		g.setTitle("Xác nhận giao dịch");
		final TextView g1 = (TextView) g.findViewById(R.id.dialog2_1);
		final TextView g2 = (TextView) g.findViewById(R.id.dialog2_2);
		final TextView g3 = (TextView) g.findViewById(R.id.dialog2_3);
		Button g4 = (Button) g.findViewById(R.id.dialog2_4);
		Button g5 = (Button) g.findViewById(R.id.dialog2_5);

		editPIN.setOnHoverListener(new View.OnHoverListener() {

			@Override
			public boolean onHover(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				check.setChecked(false);
				btn_Finish.setEnabled(false);
				return true;
			}
		});
		spin1.setOnHoverListener(new View.OnHoverListener() {

			@Override
			public boolean onHover(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				check.setChecked(false);
				btn_Finish.setEnabled(false);
				return true;
			}
		});
		spin2.setOnHoverListener(new View.OnHoverListener() {

			@Override
			public boolean onHover(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				check.setChecked(false);
				btn_Finish.setEnabled(false);
				return true;
			}
		});
		
		b1.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				d.dismiss();
			}
		});
		b2.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				d.dismiss();
				Intent intentChat2 = new Intent(Refill.this, Chat.class);
				startActivity(intentChat2);
			}
		});

		btnCheck.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				if (checkMoney() == true && checkPIN() == true) {
					check.setChecked(true);
					btn_Finish.setEnabled(true);
				} else {

					d1.setVisibility(View.GONE);
					if (checkMoney() == false) {
						d2.setText("- Tài khoản không đủ tiền");
						d2.setVisibility(View.VISIBLE);
					} else
						d2.setVisibility(View.GONE);
					if (checkPIN() == false) {
						d3.setText("- Sai Mã PIN");
						d3.setVisibility(View.VISIBLE);
					} else
						d3.setVisibility(View.GONE);

					d.show();
				}
			}
		});
		g5.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				g.dismiss();
			}
		});
		g4.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Account temp1 = new Account();
				for (int i = 0; i < arrAccount.size(); i++) {
					if (arrAccount.get(i).getUser().equals(s)) {
						String j = ""
								+ (Integer.parseInt(arrAccount.get(i)
										.getMoney()) - Integer.parseInt(spin2
										.getSelectedItem().toString()));
						temp1.setAddress(arrAccount.get(i).getAddress());
						temp1.setMoney(j);
						temp1.setName(arrAccount.get(i).getName());
						temp1.setPhone(arrAccount.get(i).getPhone());
						temp1.setPIN(arrAccount.get(i).getPIN());

					}
				}
				dbHelper.updateAccount(temp1, s);
				String currentDateandTime;
				SimpleDateFormat sdf = new SimpleDateFormat(
						"HH:mm:ss dd-MM-yyyy");
				currentDateandTime = sdf.format(new Date());
				db2.add("Nạp thẻ điện thoại "
						+ spin1.getSelectedItem().toString(), spin2
						.getSelectedItem().toString(), currentDateandTime, s);
				Exchange temp3 = new Exchange();
				for (int i = 0; i < arrExchange.size(); i++) {
					if (arrExchange.get(i).getUser().equals(s)) {
						temp3.setTime(currentDateandTime);
						temp3.setMoney(spin2.getSelectedItem().toString());
					}
				}
				dbHelper3.updateExchange(temp3, s);
				g.dismiss();
				AlertDialog.Builder b = new AlertDialog.Builder(Refill.this);
				b.setTitle("Thông báo");
				b.setMessage("Nạp thẻ thành công");
				b.setPositiveButton("Xem hóa đơn",
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								Intent intentChange = new Intent(Refill.this,
										Bills.class);
								Bundle bundle5 = new Bundle();
								bundle5.putString("user", s);
								intentChange.putExtra("IntentBills", bundle5);
								startActivity(intentChange);
							}
						});

				b.setNegativeButton("Tiếp tục",
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which)

							{
							}

						});

				b.create().show();
			}
		});
		btn_Finish.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				g1.setText("- Nhà mạng: "+spin1.getSelectedItem().toString());
				g2.setText("- Mệnh giá: " + spin2.getSelectedItem().toString());
				g3.setVisibility(View.GONE);
				g.show();
			}
		});

	}

	public void launchRingDialog(View view) {
		final ProgressDialog ringProgressDialog = ProgressDialog.show(
				Refill.this, "Xong vui lòng đợi", "Đang xử lý...", true);
		ringProgressDialog.setCancelable(true);
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {

					Thread.sleep(3000);
				} catch (Exception e) {

				}
				ringProgressDialog.dismiss();
			}
		}).start();

	}

	private class MyProcessEvent1 implements OnItemSelectedListener {
		public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
				long arg3) {
		}

		public void onNothingSelected(AdapterView<?> arg0) {
		}
	}

	private class MyProcessEvent2 implements OnItemSelectedListener {
		public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
				long arg3) {
		}

		public void onNothingSelected(AdapterView<?> arg0) {
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.logout, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			finish();
			return true;
		case R.id.logout:
			finish();
			Intent quit = new Intent(Refill.this, Login.class);
			quit.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(quit);
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	public boolean checkPIN() {
		arrAccount = dbHelper.viewAll();
		for (int i = 0; i < arrAccount.size(); i++) {
			if (arrAccount.get(i).getUser().equals(s)) {
				if (editPIN.getText().toString()
						.equals(arrAccount.get(i).getPIN())) {
					return true;
				}
			}
		}
		return false;
	}

	public boolean checkMoney() {
		arrAccount = dbHelper.viewAll();
		int a = 0;
		if (spin2.getSelectedItem().toString().equals(""))
			a = 0;
		else
			a = Integer.parseInt(spin2.getSelectedItem().toString());
		for (int i = 0; i < arrAccount.size(); i++) {
			if (arrAccount.get(i).getUser().equals(s)) {
				if (a <= Integer.parseInt(arrAccount.get(i).getMoney())) {
					return true;
				}
			}
		}
		return false;
	}

	protected void onDestroy() {
		super.onDestroy();
		db2.close();
		dbHelper.close();
		dbHelper3.close();
	}

	public void Noti() {
		Toast toast = Toast.makeText(this, "Sai thông tin. Vui lòng nhập lại",
				Toast.LENGTH_SHORT);
		toast.show();
	}

	public void doToast() {
		Toast toast = Toast.makeText(this, "Nạp thẻ thành công",
				Toast.LENGTH_SHORT);
		toast.show();
	}

}
