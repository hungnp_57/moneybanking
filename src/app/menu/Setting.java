package app.menu;

import java.util.ArrayList;

import listviewpayments.DataHelperSetting;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.TextView;
import android.widget.Toast;
import app.main.Login;
import app.main.R;

public class Setting extends Activity {
	TextView tv_setting;
	CheckBox cb1, cb2, cb3, cb4, cb5, cb6;
	DataHelperSetting dbHelper;
	String s;
	ArrayList<listviewpayments.Setting> arrSetting;

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.setting);
		tv_setting = (TextView) findViewById(R.id.tv_setting);
		cb1 = (CheckBox) findViewById(R.id.cb_setting1);
		cb2 = (CheckBox) findViewById(R.id.cb_setting2);
		cb3 = (CheckBox) findViewById(R.id.cb_setting3);
		cb4 = (CheckBox) findViewById(R.id.cb_setting4);
		cb5 = (CheckBox) findViewById(R.id.cb_setting5);
		cb6 = (CheckBox) findViewById(R.id.cb_setting6);
		dbHelper = new DataHelperSetting(Setting.this);
		arrSetting = dbHelper.viewAll();
		Intent callerIntent = getIntent();
		Bundle packageFromCaller = callerIntent.getBundleExtra("IntentSetting");
		s = packageFromCaller.getString("user");
		for (int i = 0; i < arrSetting.size(); i++) {
			if (s.equals(arrSetting.get(i).getUser())) {
				if (arrSetting.get(i).getS1().equals("Yes")) {
					cb1.setChecked(true);
					tv_setting.setText("On");
				} else {
					cb1.setChecked(false);
					tv_setting.setText("Off");
				}
				if (arrSetting.get(i).getS2().equals("Yes"))
					cb2.setChecked(true);
				if (arrSetting.get(i).getS3().equals("Yes"))
					cb3.setChecked(true);
				if (arrSetting.get(i).getS4().equals("Yes"))
					cb4.setChecked(true);
				if (arrSetting.get(i).getS5().equals("Yes"))
					cb5.setChecked(true);
				if (arrSetting.get(i).getS6().equals("Yes"))
					cb6.setChecked(true);
			}
		}
		cb1.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// TODO Auto-generated method stub
				if(isChecked) tv_setting.setText("On");
				else tv_setting.setText("Off");
				
			}
		});

		ActionBar actionBar = getActionBar();
		getActionBar().setBackgroundDrawable(
				new ColorDrawable(Color.parseColor("#3cb879")));
		if (actionBar != null) {
			actionBar.setTitle("Thiết lập");
			actionBar.setDisplayHomeAsUpEnabled(true);

		}

	}
	protected void onDestroy() {
		super.onDestroy();
		dbHelper.close();
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.menu_setting, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			finish();
			return true;
		case R.id.logout:
			finish();
			Intent quit=new Intent(Setting.this, Login.class);
			quit.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(quit);
			return true;
		case R.id.save:
			listviewpayments.Setting temp = new listviewpayments.Setting();
			if (cb1.isChecked())
				temp.setS1("Yes");
			else
				temp.setS1("No");
			if (cb2.isChecked())
				temp.setS2("Yes");
			else
				temp.setS2("No");
			if (cb3.isChecked())
				temp.setS3("Yes");
			else
				temp.setS3("No");
			if (cb4.isChecked())
				temp.setS4("Yes");
			else
				temp.setS4("No");
			if (cb5.isChecked())
				temp.setS5("Yes");
			else
				temp.setS5("No");
			if (cb6.isChecked())
				temp.setS6("Yes");
			else
				temp.setS6("No");
			dbHelper.updateSetting(temp, s);
			Toast.makeText(getApplicationContext(),
					"Thiết lập đã được lưu", Toast.LENGTH_SHORT)
					.show();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}
}
