package app.menu;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import listviewpayments.Account;
import listviewpayments.DataHelper;
import listviewpayments.DataHelperAccount;
import listviewpayments.DataHelperExchange;
import listviewpayments.Exchange;
import android.app.ActionBar;
import android.app.Activity;
import android.app.Dialog;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import app.main.Login;
import app.main.R;

public class Transfer extends Activity {
	private DataHelperAccount dbHelper;
	private DataHelper dbHelper2;
	private DataHelperExchange dbHelper3;
	ArrayList<Account> arrAccount;
	ArrayList<Exchange> arrExchange;
	String currentDateandTime;
	EditText editUser;
	EditText editPIN;
	EditText editMoney;
	EditText editDescription;
	Button btnFinish;
	String s, time, date;
	Calendar cal;

	protected void onCreate(Bundle savedInstanceState) {
		ActionBar actionBar = getActionBar();
		if (actionBar != null) {
			actionBar.setTitle("Chuyển tiền");
			actionBar.setDisplayHomeAsUpEnabled(true);
		}
		getActionBar().setBackgroundDrawable(
				new ColorDrawable(Color.parseColor("#3cb879")));

		super.onCreate(savedInstanceState);
		setContentView(R.layout.transfer);
		dbHelper = new DataHelperAccount(Transfer.this);
		dbHelper2 = new DataHelper(Transfer.this);
		dbHelper3 = new DataHelperExchange(Transfer.this);
		arrAccount = dbHelper.viewAll();
		arrExchange = dbHelper3.viewAll();
		cal = Calendar.getInstance();
		// SimpleDateFormat dft = null;
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss dd-MM-yyyy");
		currentDateandTime = sdf.format(new Date());

		Intent callerIntent = getIntent();
		Bundle packageFromCaller = callerIntent
				.getBundleExtra("IntentTransfer");
		s = packageFromCaller.getString("user");

		Button btn_Test = (Button) findViewById(R.id.btnTest);
		final Button btn_Finish = (Button) findViewById(R.id.btnFinish);
		final CheckBox check = (CheckBox) findViewById(R.id.checkMoney);
		editUser = (EditText) findViewById(R.id.transfer_user);

		editMoney = (EditText) findViewById(R.id.transfer_money);
		editPIN = (EditText) findViewById(R.id.transfer_PIN);
		editDescription = (EditText) findViewById(R.id.transfer_Description);

		final Dialog d = new Dialog(Transfer.this);
		d.setContentView(R.layout.dialog);
		d.setTitle("Lỗi giao dịch");
		final TextView d1 = (TextView) d.findViewById(R.id.dialog1_1);
		final TextView d3 = (TextView) d.findViewById(R.id.dialog1_3);
		final TextView d2 = (TextView) d.findViewById(R.id.dialog1_2);
		Button b1 = (Button) d.findViewById(R.id.dialog1_4);
		Button b2 = (Button) d.findViewById(R.id.dialog1_5);

		final Dialog g = new Dialog(Transfer.this);
		g.setContentView(R.layout.dialog2);
		g.setTitle("Xác nhận giao dịch");
		final TextView g1 = (TextView) g.findViewById(R.id.dialog2_1);
		final TextView g2 = (TextView) g.findViewById(R.id.dialog2_2);
		final TextView g3 = (TextView) g.findViewById(R.id.dialog2_3);
		Button g4 = (Button) g.findViewById(R.id.dialog2_4);
		Button g5 = (Button) g.findViewById(R.id.dialog2_5);

		editUser.setOnHoverListener(new View.OnHoverListener() {

			@Override
			public boolean onHover(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				check.setChecked(false);
				btn_Finish.setEnabled(false);
				return true;
			}
		});
		editMoney.setOnHoverListener(new View.OnHoverListener() {

			@Override
			public boolean onHover(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				check.setChecked(false);
				btn_Finish.setEnabled(false);
				return true;
			}
		});
		editPIN.setOnHoverListener(new View.OnHoverListener() {

			@Override
			public boolean onHover(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				check.setChecked(false);
				btn_Finish.setEnabled(false);
				return true;
			}
		});
		editDescription.setOnHoverListener(new View.OnHoverListener() {

			@Override
			public boolean onHover(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				check.setChecked(false);
				btn_Finish.setEnabled(false);
				return true;
			}
		});
		b1.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				d.dismiss();
			}
		});
		b2.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				d.dismiss();
				Intent intentChat2 = new Intent(Transfer.this, Chat.class);
				startActivity(intentChat2);
			}
		});

		btn_Test.setOnClickListener(new View.OnClickListener() {

			// @Override
			public void onClick(View v) {
				if (checkUser() == true && checkMoney() == true
						&& checkPIN() == true) {
					check.setChecked(true);
					btn_Finish.setEnabled(true);
				} else {

					if (checkUser() == false) {
						d1.setText("- Tài khoản không tồn tại");
						d1.setVisibility(View.VISIBLE);

					} else
						d1.setVisibility(View.GONE);
					if (checkMoney() == false) {
						d2.setText("- Tài khoản không đủ tiền");
						d2.setVisibility(View.VISIBLE);
					} else
						d2.setVisibility(View.GONE);
					if (checkPIN() == false) {
						d3.setText("- Sai Mã PIN");
						d3.setVisibility(View.VISIBLE);
					} else
						d3.setVisibility(View.GONE);

					d.show();
				}

			}
		});

		g4.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Account temp1 = new Account();
				Account temp2 = new Account();
				int money1 = 0, money2 = 0;
				for (int i = 0; i < arrAccount.size(); i++) {
					if (arrAccount.get(i).getUser().equals(s)) {
						money1 = Integer.parseInt(arrAccount.get(i).getMoney())
								- Integer.parseInt(editMoney.getText()
										.toString());
						String j = "" + money1;
						temp1.setMoney(j);
						temp1.setAddress(arrAccount.get(i).getAddress());
						temp1.setName(arrAccount.get(i).getName());
						temp1.setPhone(arrAccount.get(i).getPhone());
						temp1.setPIN(arrAccount.get(i).getPIN());
					}
				}
				for (int i = 0; i < arrAccount.size(); i++) {
					if (arrAccount.get(i).getUser()
							.equals(editUser.getText().toString())) {
						money2 = Integer.parseInt(arrAccount.get(i).getMoney())
								+ Integer.parseInt(editMoney.getText()
										.toString());
						String k = "" + money2;
						temp2.setMoney(k);
						temp2.setAddress(arrAccount.get(i).getAddress());
						temp2.setName(arrAccount.get(i).getName());
						temp2.setPhone(arrAccount.get(i).getPhone());
						temp2.setPIN(arrAccount.get(i).getPIN());
					}
				}
				dbHelper.updateAccount(temp1, s);
				dbHelper.updateAccount(temp2, editUser.getText().toString());
				SimpleDateFormat sdf = new SimpleDateFormat(
						"HH:mm:ss dd-MM-yyyy");
				currentDateandTime = sdf.format(new Date());
				dbHelper2.add("Chuyển tiền đến "
						+ editUser.getText().toString(), editMoney.getText()
						.toString(), currentDateandTime, s);
				Exchange temp3 = new Exchange();
				for (int i = 0; i < arrExchange.size(); i++) {
					if (arrExchange.get(i).getUser().equals(s)) {
						temp3.setTime(currentDateandTime);
						temp3.setMoney(editMoney.getText().toString());
					}
				}
				dbHelper3.updateExchange(temp3, s);
				g.dismiss();
				AlertDialog.Builder b = new AlertDialog.Builder(Transfer.this);
				b.setTitle("Thông báo");
				b.setMessage("Chuyển tiền thành công");
				b.setPositiveButton("Xem hóa đơn",
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								Intent intentChange = new Intent(Transfer.this,
										Bills.class);
								Bundle bundle5 = new Bundle();
								bundle5.putString("user", s);
								intentChange.putExtra("IntentBills", bundle5);
								startActivity(intentChange);
							}
						});

				b.setNegativeButton("Tiếp tục",
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which)

							{
							}

						});

				b.create().show();

			}
		});
		g5.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				g.dismiss();
			}
		});
		btn_Finish.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				String s = "";
				for (int i = 0; i < arrAccount.size(); i++) {
					if (arrAccount.get(i).getUser()
							.equals(editUser.getText().toString())) {
						s = arrAccount.get(i).getName();
					}
				}
				;
				g1.setText("- Chuyển tới: " + s);
				g2.setText("- Mã số tài khoản: "
						+ editUser.getText().toString());
				g3.setText("- Số tiền: " + editMoney.getText().toString());
				g.show();

			}
		});

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.logout, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			finish();
			return true;
		case R.id.logout:
			finish();
			Intent quit = new Intent(Transfer.this, Login.class);
			quit.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(quit);
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	protected void onDestroy() {
		super.onDestroy();
		dbHelper2.close();
		dbHelper.close();
		dbHelper3.close();
	}

	public void Noti() {
		Toast toast = Toast.makeText(this, "Sai thông tin vui lòng nhập lại!",
				Toast.LENGTH_SHORT);
		toast.show();
	}

	public boolean checkUser() {
		arrAccount = dbHelper.viewAll();
		for (int i = 0; i < arrAccount.size(); i++) {
			if (arrAccount.get(i).getUser()
					.equals(editUser.getEditableText().toString())
					&& arrAccount.get(i).getUser().equals(s) == false) {
				return true;
			}
		}
		return false;
	}

	public boolean checkMoney() {
		arrAccount = dbHelper.viewAll();
		int a = 0;
		if (editMoney.getText().toString().equals(""))
			a = 0;
		else
			a = Integer.parseInt(editMoney.getText().toString());
		for (int i = 0; i < arrAccount.size(); i++) {
			if (arrAccount.get(i).getUser().equals(s)) {
				if (a <= Integer.parseInt(arrAccount.get(i).getMoney())) {
					return true;
				}
			}
		}
		return false;
	}

	public boolean checkPIN() {
		arrAccount = dbHelper.viewAll();
		for (int i = 0; i < arrAccount.size(); i++) {
			if (arrAccount.get(i).getUser().equals(s)) {
				if (editPIN.getText().toString()
						.equals(arrAccount.get(i).getPIN())) {
					return true;
				}
			}
		}
		return false;
	}
}