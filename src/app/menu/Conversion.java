package app.menu;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import listviewpayments.DataHelperTygia;
import listviewpayments.Tygia;
import android.app.ActionBar;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.RadioButton;
import android.widget.TextView;
import app.main.Login;
import app.main.R;

public class Conversion extends Activity {
	TextView txtDate;
	Button btnDate;
	Calendar cal;
	TextView r1;
	TextView r2;
	TextView r3;
	TextView r4;
	TextView r5;
	TextView r6;
	TextView r7;
	TextView r8;
	TextView r9;
	TextView r10;
	TextView r11;
	TextView r12;
	TextView r13;
	TextView r14;
	TextView r15;
	TextView r16;
	TextView r17;
	TextView r18;
	TextView r19;
	TextView r20;
	TextView r21;
	TextView r22;
	TextView r23;
	TextView r24;
	TextView r25;
	TextView r26;
	TextView r27;
	TextView r28;
	TextView r29;
	TextView r30;
	TextView r31;
	TextView r32;
	TextView r33;
	TextView r34;
	TextView r35;
	TextView r36;
	Button btnNext, btnPre;
	private MenuItem menuItem;
	private DataHelperTygia dbHelper;
	ArrayList<Tygia> arrTygia;
	TextView lastupdate;

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.conversion);
		/*
		 * ActionBar bar = getActionBar(); Drawable
		 * d=getResources().getDrawable(R.drawable.inbox_white);
		 * bar.setBackgroundDrawable(d);
		 */
		txtDate = (TextView) findViewById(R.id.txtdate);
		btnDate = (Button) findViewById(R.id.btndate);
		btnPre = (Button) findViewById(R.id.btnpre);
		btnNext = (Button) findViewById(R.id.btnnext);
		lastupdate = (TextView) findViewById(R.id.lastupdate);

		Calendar calendar = Calendar.getInstance();
		Date date = calendar.getTime();
		SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss dd-MM-yyyy");
		String formattedDate = format.format(date);
		lastupdate.setText("Cập nhật cuối " + formattedDate);

		r1 = (TextView) findViewById(R.id.r1);
		r2 = (TextView) findViewById(R.id.r2);
		r3 = (TextView) findViewById(R.id.r3);
		r4 = (TextView) findViewById(R.id.r4);
		r5 = (TextView) findViewById(R.id.r5);
		r6 = (TextView) findViewById(R.id.r6);
		r7 = (TextView) findViewById(R.id.r7);
		r8 = (TextView) findViewById(R.id.r8);
		r9 = (TextView) findViewById(R.id.r9);
		r10 = (TextView) findViewById(R.id.r10);
		r11 = (TextView) findViewById(R.id.r11);
		r12 = (TextView) findViewById(R.id.r12);
		r13 = (TextView) findViewById(R.id.r13);
		r14 = (TextView) findViewById(R.id.r14);
		r15 = (TextView) findViewById(R.id.r15);
		r16 = (TextView) findViewById(R.id.r16);
		r17 = (TextView) findViewById(R.id.r17);
		r18 = (TextView) findViewById(R.id.r18);
		r19 = (TextView) findViewById(R.id.r19);
		r20 = (TextView) findViewById(R.id.r20);
		r21 = (TextView) findViewById(R.id.r21);
		r22 = (TextView) findViewById(R.id.r22);
		r23 = (TextView) findViewById(R.id.r23);
		r24 = (TextView) findViewById(R.id.r24);
		r25 = (TextView) findViewById(R.id.r25);
		r26 = (TextView) findViewById(R.id.r26);
		r27 = (TextView) findViewById(R.id.r27);
		r28 = (TextView) findViewById(R.id.r28);
		r29 = (TextView) findViewById(R.id.r29);
		r30 = (TextView) findViewById(R.id.r30);
		r31 = (TextView) findViewById(R.id.r31);
		r32 = (TextView) findViewById(R.id.r32);
		r33 = (TextView) findViewById(R.id.r33);
		r34 = (TextView) findViewById(R.id.r34);
		r35 = (TextView) findViewById(R.id.r35);
		r36 = (TextView) findViewById(R.id.r36);
		
		btnDate.setOnClickListener(new MyButtonEvent());
		cal = Calendar.getInstance();
		SimpleDateFormat dft = null;
		dft = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
		String strDate = dft.format(cal.getTime());
		txtDate.setText(strDate);
		ActionBar actionBar = getActionBar();
		if (actionBar != null) {
			actionBar.setTitle("Tỷ giá");
			actionBar.setDisplayHomeAsUpEnabled(true);

		}
		getActionBar().setBackgroundDrawable(
				new ColorDrawable(Color.parseColor("#3cb879")));
		btnNext.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy", Locale
						.getDefault());
				Date date1 = new Date();
				try {
					date1 = df.parse(txtDate.getText().toString());
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				Calendar cal = Calendar.getInstance();
				cal.setTime(date1);
				cal.add(Calendar.DAY_OF_MONTH, 1); // add 28 days
				date1 = (Date) cal.getTime();
				String date2 = df.format(date1);
				txtDate.setText(date2);
				listTake();
			}
		});
		btnPre.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy", Locale
						.getDefault());
				Date date1 = new Date();
				try {
					date1 = df.parse(txtDate.getText().toString());
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				Calendar cal = Calendar.getInstance();
				cal.setTime(date1);
				cal.add(Calendar.DAY_OF_MONTH, -1); 
				date1 = (Date) cal.getTime();
				String date2 = df.format(date1);
				txtDate.setText(date2);
				listTake();
			}
		});
		dbHelper = new DataHelperTygia(Conversion.this);

		arrTygia = dbHelper.viewAll();
		listTake();
	}

	public void listTake() {

		arrTygia = dbHelper.viewAll();
		int dem = 0;
		for (int i = 0; i < arrTygia.size(); i++) {

			if (arrTygia.get(i).getDate().equals(txtDate.getText())) {
				r1.setText(arrTygia.get(i).getR1());
				r2.setText(arrTygia.get(i).getR2());
				r3.setText(arrTygia.get(i).getR3());
				r4.setText(arrTygia.get(i).getR4());
				r5.setText(arrTygia.get(i).getR5());
				r6.setText(arrTygia.get(i).getR6());
				r7.setText(arrTygia.get(i).getR7());
				r8.setText(arrTygia.get(i).getR8());
				r9.setText(arrTygia.get(i).getR9());
				r10.setText(arrTygia.get(i).getR10());
				r11.setText(arrTygia.get(i).getR11());
				r12.setText(arrTygia.get(i).getR12());
				r13.setText(arrTygia.get(i).getR13());
				r14.setText(arrTygia.get(i).getR14());
				r15.setText(arrTygia.get(i).getR15());
				r16.setText(arrTygia.get(i).getR16());
				r17.setText(arrTygia.get(i).getR17());
				r18.setText(arrTygia.get(i).getR18());
				r19.setText(arrTygia.get(i).getR19());
				r20.setText(arrTygia.get(i).getR20());
				r21.setText(arrTygia.get(i).getR21());
				r22.setText(arrTygia.get(i).getR22());
				r23.setText(arrTygia.get(i).getR23());
				r24.setText(arrTygia.get(i).getR24());
				r25.setText(arrTygia.get(i).getR25());
				r26.setText(arrTygia.get(i).getR26());
				r27.setText(arrTygia.get(i).getR27());
				r28.setText(arrTygia.get(i).getR28());
				r29.setText(arrTygia.get(i).getR29());
				r30.setText(arrTygia.get(i).getR30());
				r31.setText(arrTygia.get(i).getR31());
				r32.setText(arrTygia.get(i).getR32());
				r33.setText(arrTygia.get(i).getR33());
				r34.setText(arrTygia.get(i).getR34());
				r35.setText(arrTygia.get(i).getR35());
				r36.setText(arrTygia.get(i).getR36());
			} else
				dem++;
		}
		if (dem == arrTygia.size()) {
			r1.setText("updating...");
			r2.setText("updating...");
			r3.setText("updating...");
			r4.setText("updating...");
			r5.setText("updating...");
			r6.setText("updating...");
			r7.setText("updating...");
			r8.setText("updating...");
			r9.setText("updating...");
			r10.setText("updating...");
			r11.setText("updating...");
			r12.setText("updating...");
			r13.setText("updating...");
			r14.setText("updating...");
			r15.setText("updating...");
			r16.setText("updating...");
			r17.setText("updating...");
			r18.setText("updating...");
			r19.setText("updating...");
			r20.setText("updating...");
			r21.setText("updating...");
			r22.setText("updating...");
			r23.setText("updating...");
			r24.setText("updating...");
			r25.setText("updating...");
			r26.setText("updating...");
			r27.setText("updating...");
			r28.setText("updating...");
			r29.setText("updating...");
			r30.setText("updating...");
			r31.setText("updating...");
			r32.setText("updating...");
			r33.setText("updating...");
			r34.setText("updating...");
			r35.setText("updating...");
			r36.setText("updating...");
		}

	}

	private class MyButtonEvent implements OnClickListener {
		@Override
		public void onClick(View v) {
			int i = v.getId();
			if (i == R.id.btndate) {
				showDatePickerDialog();
			}
		}

	}

	public void showDatePickerDialog() {
		OnDateSetListener callback = new OnDateSetListener() {
			public void onDateSet(DatePicker view, int year, int monthOfYear,
					int dayOfMonth) {
				if (dayOfMonth < 10) {
					txtDate.setText("0" + (dayOfMonth) + "/"
							+ (monthOfYear + 1) + "/" + year);

				} else {
					txtDate.setText((dayOfMonth) + "/" + (monthOfYear + 1)
							+ "/" + year);
				}
				listTake();
			}
		};
		String s = txtDate.getText() + "";
		String strArrtmp[] = s.split("/");
		int ngay = Integer.parseInt(strArrtmp[0]);
		int thang = Integer.parseInt(strArrtmp[1]) - 1;
		int nam = Integer.parseInt(strArrtmp[2]);
		DatePickerDialog pic = new DatePickerDialog(Conversion.this, callback,
				nam, thang, ngay);
		pic.setTitle("Chọn ngày");
		pic.show();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int i = item.getItemId();

		if (i == android.R.id.home) {
			finish();
			return true;
		} else if (i == R.id.menu_load) {
			menuItem = item;
			menuItem.setActionView(R.layout.progressbar);
			menuItem.expandActionView();
			TestTask task = new TestTask();
			task.execute("test");
			listTake();
			Calendar calendar = Calendar.getInstance();
			Date date = calendar.getTime();
			SimpleDateFormat format = new SimpleDateFormat(
					"HH:mm:ss dd-MM-yyyy");
			String formattedDate = format.format(date);
			lastupdate.setText("Cập nhật cuối " + formattedDate);
			return true;
		} else if (i == R.id.logout) {
			finish();
			Intent quit = new Intent(Conversion.this, Login.class);
			quit.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(quit);
			return true;
		} else {
			return super.onOptionsItemSelected(item);
		}
	}

	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.menu_conversion, menu);
		return true;
	}

	private class TestTask extends AsyncTask<String, Void, String> {

		@Override
		protected String doInBackground(String... params) {
			// Simulate something long running
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			menuItem.collapseActionView();
			menuItem.setActionView(null);
		}
	}

	protected void onDestroy() {
		super.onDestroy();
		dbHelper.close();
	}
}
