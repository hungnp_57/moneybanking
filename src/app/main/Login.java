package app.main;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Vector;

import listviewpayments.Account;
import listviewpayments.DataHelper;
import listviewpayments.DataHelperAccount;
import listviewpayments.DataHelperExchange;
import listviewpayments.DataHelperPaymentDetail;
import listviewpayments.DataHelperPlace;
import listviewpayments.DataHelperSetting;
import listviewpayments.DataHelperTygia;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;
import app.menu.GridViewExample;

public class Login extends Activity {
	private DataHelperAccount dbHelper;
	private DataHelper db2;
	private DataHelperPaymentDetail db3;
	private DataHelperPlace db4;
	private DataHelperTygia db5;
	private DataHelperSetting db6;
	private DataHelperExchange db7;
	Button btopen;
	ArrayList<Account> arrAccount;
	Vector<String> arrUser = new Vector<String>();
	Vector<String> arrPIN = new Vector<String>();

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login);
		dbHelper = new DataHelperAccount(Login.this);
		db2 = new DataHelper(Login.this);
		db3 = new DataHelperPaymentDetail(Login.this);
		db4 = new DataHelperPlace(Login.this);
		db5 = new DataHelperTygia(Login.this);
		db6 = new DataHelperSetting(Login.this);
		db7 = new DataHelperExchange(Login.this);
		arrAccount = dbHelper.viewAll();
		ImageView img_animation = (ImageView) findViewById(R.id.imgLogo);
		Animation myRotation = AnimationUtils.loadAnimation(
				getApplicationContext(), R.anim.rotator);
		img_animation.startAnimation(myRotation);
		if (arrAccount.size() == 0)
			resetData();
		
		db5.clear();
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat dft = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
		String strDate = dft.format(cal.getTime());
		Date date1 = new Date();
		Date date3 = new Date();
		cal.add(Calendar.DAY_OF_MONTH, -1);
		date1 = (Date) cal.getTime();
		String date2 = dft.format(date1);
		cal.add(Calendar.DAY_OF_MONTH, -1);
		date3=(Date) cal.getTime();
		String date4 = dft.format(date3);
		
		db5.add(strDate, "21,500.00", "21,500.00", "21,500.00",
				"21,500.00", "21,500.00", "21,500.00", "21,500.00",
				"2,300.00", "21,500.00", "21,500.00", "21,500.00",
				"21,500.00", "21,500.00", "21,500.00", "21,500.00",
				"21,500.00", "21,500.00", "21,500.00", "21,500.00",
				"21,500.00", "21,500.00", "21,500.00", "21,500.00",
				"21,500.00", "21,500.00", "21,500.00", "21,500.00",
				"21,500.00", "21,500.00", "21,500.00", "21,500.00",
				"21,500.00", "21,500.00", "21,500.00", "21,500.00", "21,500.00");
		db5.add(date2, "21,500.00", "21,500.00", "21,500.00",
				"21,500.00", "21,500.00", "21,500.00", "21,500.00",
				"21,500.00", "21,500.00", "21,500.00", "21,500.00",
				"21,500.00", "21,500.00", "21,500.00", "21,500.00",
				"21,500.00", "21,500.00", "21,500.00", "21,500.00",
				"21,500.00", "21,500.00", "21,500.00", "21,500.00",
				"21,500.00", "21,500.00", "21,500.00", "21,500.00",
				"21,500.00", "21,500.00", "21,500.00", "21,500.00",
				"21,500.00", "21,500.00", "21,500.00", "21,500.00", "21,500.00");
		db5.add(date4, "21,500.00", "21,500.00", "21,500.00",
				"21,500.00", "21,500.00", "21,500.00", "21,500.00",
				"21,500.00", "21,500.00", "21,500.00", "21,500.00",
				"21,500.00", "21,500.00", "21,500.00", "21,500.00",
				"21,500.00", "21,500.00", "21,500.00", "21,500.00",
				"21,500.00", "21,500.00", "21,500.00", "21,500.00",
				"21,500.00", "21,500.00", "21,500.00", "21,500.00",
				"21,500.00", "21,500.00", "21,500.00", "21,500.00",
				"21,500.00", "21,500.00", "21,500.00", "21,500.00", "21,500.00");
		
		
		
		btopen = (Button) findViewById(R.id.bt_Connect);

		for (int i = 0; i < arrAccount.size(); i++) {
			arrUser.add(arrAccount.get(i).getUser());
			arrPIN.add(arrAccount.get(i).getPIN());
		}

		btopen.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				EditText tv1 = (EditText) findViewById(R.id.ed_IPServer);
				String a = tv1.getText().toString();
				EditText tv2 = (EditText) findViewById(R.id.ed_Port);
				String b = tv2.getText().toString();
				int dem = 0;
				if (a.equals("123456") && b.equals("123456")) {
					AlertDialog.Builder c = new AlertDialog.Builder(Login.this);
					c.setTitle("Welcome Admin!");
					c.setMessage("\t\t\tReset Database?");
					c.setPositiveButton("OK",
							new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									resetData();
								}
							});

					c.setNegativeButton("Cancel",
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog,
										int which)

								{
								}

							});

					c.create().show();
					tv1.setText("");
					tv2.setText("");
				} else {

					for (int i = 0; i < arrUser.size(); i++) {
						if (a.equals(arrUser.get(i).toString())) {
							if (b.equals(arrPIN.get(i).toString())) {
								Intent myIntent = new Intent(Login.this,
										GridViewExample.class);
								Bundle bundle = new Bundle();
								String s = tv1.getText().toString();
								String t = tv2.getText().toString();
								bundle.putString("user", s);
								bundle.putString("PIN", t);
								myIntent.putExtra("LoginInfo", bundle);
								startActivity(myIntent);
							} else
								doToast();
						} else
							dem++;
					}
				}
				if (dem == arrUser.size())
					doToast();
			}
		});
		
	}

	protected void onDestroy() {
		super.onDestroy();
		db2.close();
		db3.close();
		db4.close();
		db5.close();
		dbHelper.close();
	}

	public void doOpen() {
		Intent myIntent = new Intent(this, GridViewExample.class);
		startActivity(myIntent);
	}

	public void doToast() {
		Toast toast = Toast.makeText(this, "Sai tên tài khoản hoặc mật khẩu",
				Toast.LENGTH_SHORT);
		toast.show();
	}

	public void resetData() {
		dbHelper.clear();
		db2.clear();
		db3.clear();
		db4.clear();
		db5.clear();
		db6.clear();
		dbHelper.add("12020183", "1000000", "Nguyễn Phi Hùng", "01689974330",
				"144, Xuân Thủy, Cầu Giấy, Hà Nội", "123456");
		dbHelper.add("12020184", "1000000", "Apoqliphort Killer",
				"01689974330", "175, Xuân Thủy, Cầu Giấy, Hà Nội", "123456");
		db7.add("12020183","200000", "10:30:10 24-11-2014");
		db7.add("12020184", "123000","20:30:23 25-11-2014");
		db3.add("12020183", "FPT", "250000", "No", "50", "Yes", "120", "No");
		db3.add("12020184", "VIETTEL", "120000", "No", "70", "Yes", "60", "No");
		db6.add("12020183", "Yes", "Yes", "Yes", "No", "No", "Yes");
		db6.add("12020184", "No", "Yes", "Yes", "Yes", "No", "Yes");
		db4.add("144, Xuan Thuy, Cau Giay, Ha Noi", (float) 21.0367171,
				(float) 105.782032);
		db4.add("75, Mai Dich, Cau Giay, Ha Noi", (float) 21.0393657,
				(float) 105.7742482);
		db4.add("30, Artifact, Hoan Kiem, Ha Noi", (float) 21.0272662,
				(float) 105.8554513);
		db4.add("69, Winda, Nghia Do, Ha Noi", (float) 21.0454641,
				(float) 105.8011445);
		db4.add("Time City", (float) 20.9954977, (float) 105.88005066);
		db4.add("Trang Tien Plaza", (float) 21.0251629, (float) 105.8547173);
		db4.add("Royal City", (float) 21.0028475, (float) 105.8150866);
		db4.add("Cho Xanh", (float) 21.0364279, (float) 105.7862914);
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat dft = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
		String strDate = dft.format(cal.getTime());
		Date date1 = new Date();
		Date date3 = new Date();
		cal.add(Calendar.DAY_OF_MONTH, -1); // add 28 days
		date1 = (Date) cal.getTime();
		String date2 = dft.format(date1);
		cal.add(Calendar.DAY_OF_MONTH, -1);
		date3=(Date) cal.getTime();
		String date4 = dft.format(date3);
		
		db5.add(strDate, "21,500.00", "21,500.00", "21,500.00",
				"21,500.00", "21,500.00", "21,500.00", "21,500.00",
				"2,300.00", "21,500.00", "21,500.00", "21,500.00",
				"21,500.00", "21,500.00", "21,500.00", "21,500.00",
				"21,500.00", "21,500.00", "21,500.00", "21,500.00",
				"21,500.00", "21,500.00", "21,500.00", "21,500.00",
				"21,500.00", "21,500.00", "21,500.00", "21,500.00",
				"21,500.00", "21,500.00", "21,500.00", "21,500.00",
				"21,500.00", "21,500.00", "21,500.00", "21,500.00", "21,500.00");
		db5.add(date2, "21,500.00", "21,500.00", "21,500.00",
				"21,500.00", "21,500.00", "21,500.00", "21,500.00",
				"21,500.00", "21,500.00", "21,500.00", "21,500.00",
				"21,500.00", "21,500.00", "21,500.00", "21,500.00",
				"21,500.00", "21,500.00", "21,500.00", "21,500.00",
				"21,500.00", "21,500.00", "21,500.00", "21,500.00",
				"21,500.00", "21,500.00", "21,500.00", "21,500.00",
				"21,500.00", "21,500.00", "21,500.00", "21,500.00",
				"21,500.00", "21,500.00", "21,500.00", "21,500.00", "21,500.00");
		db5.add(date4, "21,500.00", "21,500.00", "21,500.00",
				"21,500.00", "21,500.00", "21,500.00", "21,500.00",
				"21,500.00", "21,500.00", "21,500.00", "21,500.00",
				"21,500.00", "21,500.00", "21,500.00", "21,500.00",
				"21,500.00", "21,500.00", "21,500.00", "21,500.00",
				"21,500.00", "21,500.00", "21,500.00", "21,500.00",
				"21,500.00", "21,500.00", "21,500.00", "21,500.00",
				"21,500.00", "21,500.00", "21,500.00", "21,500.00",
				"21,500.00", "21,500.00", "21,500.00", "21,500.00", "21,500.00");
		arrAccount = dbHelper.viewAll();
		arrUser.clear();
		arrPIN.clear();
		for (int i = 0; i < arrAccount.size(); i++) {
			arrUser.add(arrAccount.get(i).getUser());
			arrPIN.add(arrAccount.get(i).getPIN());
		}

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.login, menu);
		return true;
	}

}
