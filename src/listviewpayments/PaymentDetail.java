package listviewpayments;

public class PaymentDetail {
	String user, supplier, money1, status1, money2, status2, money3, status3;

	public PaymentDetail() {
		super();
	}

	public PaymentDetail(String user, String supplier, String money1,
			String status1, String money2, String status2, String money3,
			String status3) {
		super();
		this.user = user;
		this.money1 = money1;
		this.money2 = money2;
		this.money3 = money3;
		this.status1 = status1;
		this.status2 = status2;
		this.status3 = status3;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getUser() {
		return this.user;
	}

	public void setSupplier(String supplier) {
		this.supplier = supplier;
	}

	public String getSupplier() {
		return this.supplier;
	}

	public void setMoney1(String money1) {
		this.money1 = money1;
	}

	public String getMoney1() {
		return this.money1;
	}

	public void setMoney2(String money2) {
		this.money2 = money2;
	}

	public String getMoney2() {
		return this.money2;
	}

	public void setMoney3(String money3) {
		this.money3 = money3;
	}

	public String getMoney3() {
		return this.money3;
	}

	public void setStatus1(String status1) {
		this.status1 = status1;
	}

	public String getStatus1() {
		return this.status1;
	}

	public void setStatus2(String status2) {
		this.status2 = status2;
	}

	public String getStatus2() {
		return this.status2;
	}

	public void setStatus3(String status3) {
		this.status3 = status3;
	}

	public String getStatus3() {
		return this.status3;
	}
}
