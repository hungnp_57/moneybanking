package listviewpayments;

import java.util.ArrayList;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DataHelperTygia extends SQLiteOpenHelper {

	public DataHelperTygia(Context context) {
		super(context, "MyDataTygia.db", null, 1);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		db.execSQL("CREATE TABLE IF NOT EXISTS Tygia(date VARCHAR,r1 VARCHAR,r2 VARCHAR,r3 VARCHAR,r4 VARCHAR,r5 VARCHAR,r6 VARCHAR,r7 VARCHAR,r8 VARCHAR,r9 VARCHAR,r10 VARCHAR,r11 VARCHAR,r12 VARCHAR,r13 VARCHAR,r14 VARCHAR,r15 VARCHAR,r16 VARCHAR,r17 VARCHAR,r18 VARCHAR,r19 VARCHAR,r20 VARCHAR,r21 VARCHAR,r22 VARCHAR,r23 VARCHAR,r24 VARCHAR,r25 VARCHAR,r26 VARCHAR,r27 VARCHAR,r28 VARCHAR,r29 VARCHAR,r30 VARCHAR,r31 VARCHAR,r32 VARCHAR,r33 VARCHAR,r34 VARCHAR,r35 VARCHAR,r36 VARCHAR);");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		db.execSQL("DROP TABLE IF EXISTS Tygia");
		onCreate(db);
	}

	public void clear() {
		SQLiteDatabase db = this.getReadableDatabase();
		db.delete("Tygia", null, null);
	}

	public void add(String date, String r1, String r2, String r3, String r4,
			String r5, String r6, String r7, String r8, String r9, String r10,
			String r11, String r12, String r13, String r14, String r15,
			String r16, String r17, String r18, String r19, String r20,
			String r21, String r22, String r23, String r24, String r25,
			String r26, String r27, String r28, String r29, String r30,
			String r31, String r32, String r33, String r34, String r35,
			String r36) {
		SQLiteDatabase db = this.getReadableDatabase();
		db.execSQL("INSERT INTO Tygia VALUES('" + date + "','" + r1 + "','"
				+ r2 + "','" + r3 + "','" + r4 + "','" + r5 + "','" + r6
				+ "','" + r7 + "','" + r8 + "','" + r9 + "','" + r10 + "','"
				+ r11 + "','" + r12 + "','" + r13 + "','" + r14 + "','" + r15
				+ "','" + r16 + "','" + r17 + "','" + r18 + "','" + r19 + "','"
				+ r20 + "','" + r21 + "','" + r22 + "','" + r23 + "','" + r24
				+ "','" + r25 + "','" + r26 + "','" + r27 + "','" + r28 + "','"
				+ r29 + "','" + r30 + "','" + r31 + "','" + r32 + "','" + r33
				+ "','" + r34 + "','" + r35 + "','" + r36 + "');");
	}

	public ArrayList<Tygia> viewAll() {
		ArrayList<Tygia> arrTygia = new ArrayList<Tygia>();
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor c = db.rawQuery("SELECT * FROM Tygia", null);
		if (c.getCount() == 0) {
			c.moveToFirst();
		}
		while (c.moveToNext()) {
			Tygia tygia = new Tygia();
			tygia.setDate(c.getString(c.getColumnIndex("date")));
			tygia.setR1(c.getString(c.getColumnIndex("r1")));
			tygia.setR2(c.getString(c.getColumnIndex("r2")));
			tygia.setR3(c.getString(c.getColumnIndex("r3")));
			tygia.setR4(c.getString(c.getColumnIndex("r4")));
			tygia.setR5(c.getString(c.getColumnIndex("r5")));
			tygia.setR6(c.getString(c.getColumnIndex("r6")));
			tygia.setR7(c.getString(c.getColumnIndex("r7")));
			tygia.setR8(c.getString(c.getColumnIndex("r8")));
			tygia.setR9(c.getString(c.getColumnIndex("r9")));
			tygia.setR10(c.getString(c.getColumnIndex("r10")));
			tygia.setR11(c.getString(c.getColumnIndex("r11")));
			tygia.setR12(c.getString(c.getColumnIndex("r12")));
			tygia.setR13(c.getString(c.getColumnIndex("r13")));
			tygia.setR14(c.getString(c.getColumnIndex("r14")));
			tygia.setR15(c.getString(c.getColumnIndex("r15")));
			tygia.setR16(c.getString(c.getColumnIndex("r16")));
			tygia.setR17(c.getString(c.getColumnIndex("r17")));
			tygia.setR18(c.getString(c.getColumnIndex("r18")));
			tygia.setR19(c.getString(c.getColumnIndex("r19")));
			tygia.setR20(c.getString(c.getColumnIndex("r20")));
			tygia.setR21(c.getString(c.getColumnIndex("r21")));
			tygia.setR22(c.getString(c.getColumnIndex("r22")));
			tygia.setR23(c.getString(c.getColumnIndex("r23")));
			tygia.setR24(c.getString(c.getColumnIndex("r24")));
			tygia.setR25(c.getString(c.getColumnIndex("r25")));
			tygia.setR26(c.getString(c.getColumnIndex("r26")));
			tygia.setR27(c.getString(c.getColumnIndex("r27")));
			tygia.setR28(c.getString(c.getColumnIndex("r28")));
			tygia.setR29(c.getString(c.getColumnIndex("r29")));
			tygia.setR30(c.getString(c.getColumnIndex("r30")));
			tygia.setR31(c.getString(c.getColumnIndex("r31")));
			tygia.setR32(c.getString(c.getColumnIndex("r32")));
			tygia.setR33(c.getString(c.getColumnIndex("r33")));
			tygia.setR34(c.getString(c.getColumnIndex("r34")));
			tygia.setR35(c.getString(c.getColumnIndex("r35")));
			tygia.setR36(c.getString(c.getColumnIndex("r36")));
			arrTygia.add(tygia);
		}
		return arrTygia;
	}

	public void closeDatabase() {
		SQLiteDatabase database = this.getWritableDatabase();

		if (database != null && database.isOpen()) {
			database.close();
		}
	}
}
