package listviewpayments;

public class Place {
	String place;
	float lat;
	float lng;

	public Place() {
		super();
	}

	public Place(String place, float lat, float lng) {
		super();
		this.place = place;
		this.lat = lat;
		this.lng = lng;
	}

	public String getPlace() {
		return this.place;
	}

	public void setPlace(String place) {
		this.place = place;
	}

	public float getLat() {
		return this.lat;
	}

	public void setLat(float lat) {
		this.lat = lat;
	}

	public float getLng() {
		return this.lng;
	}

	public void setLng(float lng) {
		this.lng = lng;
	}
}
