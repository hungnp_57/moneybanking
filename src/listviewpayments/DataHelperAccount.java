package listviewpayments;

import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DataHelperAccount extends SQLiteOpenHelper {

	public DataHelperAccount(Context context) {
		super(context, "MyDataAccount.db", null, 1);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		db.execSQL("CREATE TABLE IF NOT EXISTS Account(user VARCHAR,money VARCHAR,name VARCHAR,phone VARCHAR,address VARCHAR,PIN VARCHAR);");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		db.execSQL("DROP TABLE IF EXISTS Account");
		onCreate(db);
	}

	public void add(String user, String money, String name, String phone,
			String address, String PIN) {
		SQLiteDatabase db = this.getReadableDatabase();
		db.execSQL("INSERT INTO Account VALUES('" + user + "','" + money
				+ "','" + name + "','" + phone + "','" + address + "','" + PIN
				+ "');");
	}

	public void clear() {
		SQLiteDatabase db = this.getReadableDatabase();
		db.delete("Account", null, null);
	}

	public void updateAccount(Account account, String user) {

		SQLiteDatabase database = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put("money", account.getMoney());
		values.put("name", account.getName());
		values.put("phone", account.getPhone());
		values.put("address", account.getAddress());
		values.put("PIN", account.getPIN());
		database.update("Account", values, "user" + " = " + user, null);
	}

	public ArrayList<Account> viewAll() {
		ArrayList<Account> arrAccount = new ArrayList<Account>();
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor c = db.rawQuery("SELECT * FROM Account", null);
		if (c.getCount() == 0) {
			c.moveToFirst();
		}
		while (c.moveToNext()) {
			Account account = new Account();
			account.setUser(c.getString(c.getColumnIndex("user")));
			account.setMoney(c.getString(c.getColumnIndex("money")));
			account.setName(c.getString(c.getColumnIndex("name")));
			account.setPhone(c.getString(c.getColumnIndex("phone")));
			account.setAddress(c.getString(c.getColumnIndex("address")));
			account.setPIN(c.getString(c.getColumnIndex("PIN")));
			arrAccount.add(account);
		}
		return arrAccount;
	}

	public void closeDatabase() {
		SQLiteDatabase database = this.getWritableDatabase();

		if (database != null && database.isOpen()) {
			database.close();
		}
	}
}
