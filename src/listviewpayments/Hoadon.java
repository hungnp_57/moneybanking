package listviewpayments;

public class Hoadon {
	String name, time, money, user;

	public Hoadon() {
		super();
	}

	public Hoadon(String time, String money, String name, String user) {
		super();
		this.user = user;
		this.time = time;
		this.money = money;
		this.name = name;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getMoney() {
		return money;
	}

	public void setMoney(String money) {
		this.money = money;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
