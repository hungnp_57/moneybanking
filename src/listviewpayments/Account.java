package listviewpayments;

public class Account {
	String user, money, name, phone, address, PIN;

	public Account() {
		super();
	}

	public Account(String user, String money, String name, String phone,
			String address, String PIN) {
		super();
		this.name = name;
		this.address = address;
		this.PIN = PIN;
		this.money = money;
		this.user = user;
		this.phone = phone;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getMoney() {
		return money;
	}

	public void setMoney(String money) {
		this.money = money;
	}

	public String getPIN() {
		return PIN;
	}

	public void setPIN(String PIN) {
		this.PIN = PIN;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

}
