package listviewpayments;

import java.util.ArrayList;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DataHelperPlace extends SQLiteOpenHelper {

	public DataHelperPlace(Context context) {
		super(context, "MyDataPlace.db", null, 1);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		db.execSQL("CREATE TABLE IF NOT EXISTS Place(place VARCHAR, lat NUMERIC, lng NUMERIC);");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		db.execSQL("DROP TABLE IF EXISTS Place");
		onCreate(db);
	}

	public void add(String place, float lat, float lng) {
		SQLiteDatabase db = this.getReadableDatabase();
		db.execSQL("INSERT INTO Place VALUES('" + place + "','" + lat + "','"
				+ lng + "');");
	}

	public void clear() {
		SQLiteDatabase db = this.getReadableDatabase();
		db.delete("Place", null, null);
	}

	public ArrayList<Place> viewAll() {
		ArrayList<Place> arrPlace = new ArrayList<Place>();
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor c = db.rawQuery("SELECT * FROM Place", null);
		if (c.getCount() == 0) {
			c.moveToFirst();
		}
		while (c.moveToNext()) {
			Place place = new Place();
			place.setPlace(c.getString(c.getColumnIndex("place")));
			place.setLat(c.getFloat(c.getColumnIndex("lat")));
			place.setLng(c.getFloat(c.getColumnIndex("lng")));
			arrPlace.add(place);
		}
		return arrPlace;
	}

	public void closeDatabase() {
		SQLiteDatabase database = this.getWritableDatabase();

		if (database != null && database.isOpen()) {
			database.close();
		}
	}
}
