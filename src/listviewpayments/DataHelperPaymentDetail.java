package listviewpayments;

import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DataHelperPaymentDetail extends SQLiteOpenHelper {

	public DataHelperPaymentDetail(Context context) {
		super(context, "MyDataPaymentDetail.db", null, 1);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		db.execSQL("CREATE TABLE IF NOT EXISTS PaymentDetail(user VARCHAR,supplier VARCHAR,money1 VARCHAR,status1 VARCHAR,money2 VARCHAR,status2 VARCHAR,money3 VARCHAR,status3 VARCHAR);");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		db.execSQL("DROP TABLE IF EXISTS PaymentDetail");
		onCreate(db);
	}

	public void add(String user, String supplier, String money1,
			String status1, String money2, String status2, String money3,
			String status3) {
		SQLiteDatabase db = this.getReadableDatabase();
		db.execSQL("INSERT INTO PaymentDetail VALUES('" + user + "','"
				+ supplier + "','" + money1 + "','" + status1 + "','" + money2
				+ "','" + status2 + "','" + money3 + "','" + status3 + "');");
	}

	public void updatePaymentDetail(PaymentDetail paymentDetail, String user) {

		SQLiteDatabase database = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put("money1", paymentDetail.getMoney1());
		values.put("money2", paymentDetail.getMoney2());
		values.put("money3", paymentDetail.getMoney3());
		values.put("status1", paymentDetail.getStatus1());
		values.put("status2", paymentDetail.getStatus2());
		values.put("status3", paymentDetail.getStatus3());
		values.put("supplier", paymentDetail.getSupplier());
		database.update("PaymentDetail", values, "user" + " = " + user, null);
	}

	public void clear() {
		SQLiteDatabase db = this.getReadableDatabase();
		db.delete("PaymentDetail", null, null);
	}

	public ArrayList<PaymentDetail> viewAll() {
		ArrayList<PaymentDetail> arrPaymentDetail = new ArrayList<PaymentDetail>();
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor c = db.rawQuery("SELECT * FROM PaymentDetail", null);
		if (c.getCount() == 0) {
			c.moveToFirst();
		}
		while (c.moveToNext()) {
			PaymentDetail paymentDetail = new PaymentDetail();
			paymentDetail.setUser(c.getString(c.getColumnIndex("user")));
			paymentDetail
					.setSupplier(c.getString(c.getColumnIndex("supplier")));
			paymentDetail.setMoney1(c.getString(c.getColumnIndex("money1")));
			paymentDetail.setStatus1(c.getString(c.getColumnIndex("status1")));
			paymentDetail.setMoney2(c.getString(c.getColumnIndex("money2")));
			paymentDetail.setStatus2(c.getString(c.getColumnIndex("status2")));
			paymentDetail.setMoney3(c.getString(c.getColumnIndex("money3")));
			paymentDetail.setStatus3(c.getString(c.getColumnIndex("status3")));
			arrPaymentDetail.add(paymentDetail);
		}
		return arrPaymentDetail;
	}

	public void closeDatabase() {
		SQLiteDatabase database = this.getWritableDatabase();

		if (database != null && database.isOpen()) {
			database.close();
		}
	}
}
