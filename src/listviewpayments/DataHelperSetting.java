package listviewpayments;

import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DataHelperSetting extends SQLiteOpenHelper {

	public DataHelperSetting(Context context) {
		super(context, "MyDataSetting.db", null, 1);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		db.execSQL("CREATE TABLE IF NOT EXISTS Setting(user VARCHAR,s1 VARCHAR,s2 VARCHAR,s3 VARCHAR,s4 VARCHAR,s5 VARCHAR,s6 VARCHAR);");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		db.execSQL("DROP TABLE IF EXISTS Setting");
		onCreate(db);
	}

	public void add(String user, String s1, String s2, String s3, String s4,
			String s5, String s6) {
		SQLiteDatabase db = this.getReadableDatabase();
		db.execSQL("INSERT INTO Setting VALUES('" + user + "','" + s1 + "','"
				+ s2 + "','" + s3 + "','" + s4 + "','" + s5 + "','" + s6
				+ "');");
	}

	public void clear() {
		SQLiteDatabase db = this.getReadableDatabase();
		db.delete("Setting", null, null);
	}

	public void updateSetting(Setting setting, String user) {

		SQLiteDatabase database = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put("s1", setting.getS1());
		values.put("s2", setting.getS2());
		values.put("s3", setting.getS3());
		values.put("s4", setting.getS4());
		values.put("s5", setting.getS5());
		values.put("s6", setting.getS6());
		database.update("Setting", values, "user" + " = " + user, null);
	}

	public ArrayList<Setting> viewAll() {
		ArrayList<Setting> arrSetting = new ArrayList<Setting>();
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor c = db.rawQuery("SELECT * FROM Setting", null);
		if (c.getCount() == 0) {
			c.moveToFirst();
		}
		while (c.moveToNext()) {
			Setting setting = new Setting();
			setting.setUser(c.getString(c.getColumnIndex("user")));
			setting.setS1(c.getString(c.getColumnIndex("s1")));
			setting.setS2(c.getString(c.getColumnIndex("s2")));
			setting.setS3(c.getString(c.getColumnIndex("s3")));
			setting.setS4(c.getString(c.getColumnIndex("s4")));
			setting.setS5(c.getString(c.getColumnIndex("s5")));
			setting.setS6(c.getString(c.getColumnIndex("s6")));
			arrSetting.add(setting);
		}
		return arrSetting;
	}

	public void closeDatabase() {
		SQLiteDatabase database = this.getWritableDatabase();

		if (database != null && database.isOpen()) {
			database.close();
		}
	}
}
