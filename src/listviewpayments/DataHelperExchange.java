package listviewpayments;

import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DataHelperExchange extends SQLiteOpenHelper {

	public DataHelperExchange(Context context) {
		super(context, "MyDataExchange.db", null, 1);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		db.execSQL("CREATE TABLE IF NOT EXISTS Exchange(user VARCHAR,money VARCHAR,time VARCHAR);");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		db.execSQL("DROP TABLE IF EXISTS Exchange");
		onCreate(db);
	}

	public void add(String user,String money, String time) {
		SQLiteDatabase db = this.getReadableDatabase();
		db.execSQL("INSERT INTO Exchange VALUES('" + user + "','"+ money + "','" + time
				+ "');");
	}

	public void clear() {
		SQLiteDatabase db = this.getReadableDatabase();
		db.delete("Exchange", null, null);
	}

	public void updateExchange(Exchange exchange, String user) {

		SQLiteDatabase database = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put("money", exchange.getMoney());
		values.put("time", exchange.getTime());
		database.update("Exchange", values, "user" + " = " + user, null);
	}

	public ArrayList<Exchange> viewAll() {
		ArrayList<Exchange> arrExchange = new ArrayList<Exchange>();
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor c = db.rawQuery("SELECT * FROM Exchange", null);
		if (c.getCount() == 0) {
			c.moveToFirst();
		}
		while (c.moveToNext()) {
			Exchange exchange = new Exchange();
			exchange.setUser(c.getString(c.getColumnIndex("user")));
			exchange.setMoney(c.getString(c.getColumnIndex("money")));
			exchange.setTime(c.getString(c.getColumnIndex("time")));
			arrExchange.add(exchange);
		}
		return arrExchange;
	}

	public void closeDatabase() {
		SQLiteDatabase database = this.getWritableDatabase();

		if (database != null && database.isOpen()) {
			database.close();
		}
	}
}
