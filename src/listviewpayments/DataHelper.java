package listviewpayments;

import java.util.ArrayList;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DataHelper extends SQLiteOpenHelper {

	public DataHelper(Context context) {
		super(context, "MyData.db", null, 1);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		db.execSQL("CREATE TABLE IF NOT EXISTS Pays(name VARCHAR,money VARCHAR,time VARCHAR,user VARCHAR);");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		db.execSQL("DROP TABLE IF EXISTS Pays");
		onCreate(db);
	}

	public void add(String name, String money, String time, String user) {
		SQLiteDatabase db = this.getReadableDatabase();
		db.execSQL("INSERT INTO Pays VALUES('" + name + "','" + money + "','"
				+ time + "','" + user + "');");
	}
	
	public void delete(String time) {
		SQLiteDatabase database = this.getWritableDatabase();
		
		database.delete("Pays", "time" + " = " + "'"+time+"'", null);
	}

	public void clear() {
		SQLiteDatabase db = this.getReadableDatabase();
		db.delete("Pays", null, null);
	}

	public ArrayList<Hoadon> viewAll() {
		ArrayList<Hoadon> arrHoadon = new ArrayList<Hoadon>();
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor c = db.rawQuery("SELECT * FROM Pays", null);
		if (c.getCount() == 0) {
			c.moveToFirst();
		}
		while (c.moveToNext()) {
			Hoadon hoadon = new Hoadon();
			hoadon.setTime(c.getString(c.getColumnIndex("time")));
			hoadon.setMoney(c.getString(c.getColumnIndex("money")));
			hoadon.setName(c.getString(c.getColumnIndex("name")));
			hoadon.setUser(c.getString(c.getColumnIndex("user")));
			arrHoadon.add(hoadon);
		}
		return arrHoadon;
	}

	public void closeDatabase() {
		SQLiteDatabase database = this.getWritableDatabase();

		if (database != null && database.isOpen()) {
			database.close();
		}
	}
}
