package listviewpayments;

public class Exchange {
	String user, money,time;

	public Exchange() {
		super();
	}

	public Exchange(String user,String money, String time) {
		super();
		this.user = user;
		this.money=money;
		this.time = time;
	}
	public void setMoney(String money){
		this.money=money;
	}
	public String getMoney(){
		return this.money;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getUser() {
		return this.user;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getTime() {
		return time;
	}
}
